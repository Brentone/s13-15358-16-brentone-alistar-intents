package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;



import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openFirstScreen(View view){

        //open the first activity through an intent
        Intent intent1 = new Intent(this, First_activity.class);
        startActivity(intent1);
    }

    public void openSecondScreen(View view){

        //open the second activity through an intent
        Intent intent2 = new Intent(this, Second_activity.class);
        startActivity(intent2);
    }

    public void openThirdScreen(View view){

        //open the third activity through an intent
        Intent intent3 = new Intent(this, Third_activity.class);
        startActivity(intent3);
    }
}