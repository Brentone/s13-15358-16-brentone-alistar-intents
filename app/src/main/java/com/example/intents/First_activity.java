package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class First_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_activity);
    }

    public void backToMainScreen(View view){


        //back to main screen through an intent
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        //close the activity
        finish();


    }
}
